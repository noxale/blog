from DMP import * #Desmos Magic Printer

class formula:
	b = 0
	c = 0
	yi = True
	def __init__(self,b,c,yi):
		self.b = str(b) if b < 0 else "+"+str(b)
		self.c = str(c) if c < 0 else "+"+str(c)
		self.yi = yi
		
	def make_formula(self):
		return ("x^2 "+self.b+"x "+self.c,self.yi)


		
f_array = [[(4, 29, True),(4, 4, True),(4, 20, True),(0, 25, True),(-4, 20, True),(-4, 4, True)],[(4, 20, True),(0, 25, True),(-4, 20, True),(-4, 5, True),(0, 0, True),(4, 5, True),(4, 20, True)],[(4, 20, True),(-4, 4, True),(0, 4, True),(-4, 20, True),(4, 4, True)],[(-2, 2, True),(0, 0, True),(4, 4, True),(4, 13, True),(0, 16, True),(-2, 10, True),(-2, 2, True),(-4, 4, True)],[(0, 36, True),(0, 0, True)],[(4, 8, True),(0, 4, True),(-4, 13, True),(-4, 20, True),(4, 20, True),(4, 4, True),(-4, 4, True)],[(-4,40,True),(0,49,True),(-2,2,True),(2,1,True),(-2,2,False),(0,49,False),(-4,40,False)],[(4,8,False),(2,26,True),(0,9,True),(-2,26,True),(-4,8,False)],[(6,13,False),(0,100,True),(-6,13,False),(-4,8,True),(4,8,True)],[(4,29,True),(-4,29,True),(0,1,False)],[(2,26,True),(2,1,True),(2,7.25,True),(-2,7.25,True),(-2,26,True),(-2,1,True)],[(4,4,True),(-4,4,True)],[(2,26,True),(-2,26,True),(0,25,True),(0,0,True),(2,1,True),(-2,1,True)],[(-4,29,True),(4,29,True),(4,13,True),(-2,10,True),(-4,6.25,True),(-2,1,True),(4,4,True)],[(4,4,True),(-4,4,True)],[(4,8,False),(2,26,True),(0,9,True),(-2,26,True),(-4,8,False)],[(6,45,True),(2,1,True),(0,9,True),(-2,1,True),(-6,45,True)],[(4,29,True),(-2,26,True),(-4,20,True),(-2,10,True),(4,13,True),(-2,10,True),(-4,8,True),(-4,5,True),(-2,1,True),(4,4,True)],[(-4,29,True),(4,29,True),(4,13,True),(-2,10,True),(-4,6.25,True),(-2,1,True),(4,4,True)],[(0,9,True),(-4,8,True),(-4,8,False),(0,9,False),(4,8,False),(4,8,True),(0,9,True)],[(4,8,False),(2,26,True),(0,9,True),(-2,26,True),(-4,8,False)],[(4,29,True),(-2,26,True),(-4,20,True),(-2,10,True),(4,13,True),(-2,10,True),(-4,8,True),(-4,5,True),(-2,1,True),(4,4,True)],[(4, 40, True),(0, 49, True),(2, 2, True),(-2, 1, True),(2, 2, False),(0, 49, False),(4, 40, False)]]


formulas = create_formula(f_array)
letters = connect_formulas(formulas)
Desmos_Magic_Print(letters)
