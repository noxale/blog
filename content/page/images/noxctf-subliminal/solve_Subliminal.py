#! /usr/bin/python
from scapy.all import *
from scapy.layers.dhcp import DHCP
from scapy.layers.inet import IP

FLAG = ""
IP_LENGTH = 4
DHCP_TYPE_INDEX = 0
DHCP_SUBNET_MASK_INDEX = 3
DHCP_ROUTER_IP_INDEX = 4
DHCP_ACK = 5

subnet = None
server_ip = None
mask = ['-1', '-1', '-1', '-1']
common_mask_starts = ['192', '10']

print("start")
packets = rdpcap('Subliminal.pcap')

for packet in packets:
    # build subnet-mask
    if DHCP in packet:
        # check if dhcp packet is an ack type
        if packet[DHCP].options[DHCP_TYPE_INDEX][1] == DHCP_ACK:
            server_ip = packet[DHCP].options[DHCP_ROUTER_IP_INDEX][1].split('.')
            subnet = packet[DHCP].options[DHCP_SUBNET_MASK_INDEX][1].split('.')
            for i in range(IP_LENGTH):
                if subnet[i] == '255':
                    mask[i] = server_ip[i]

    elif IP in packet:
        src_ip = str(packet[IP].src)
        parts = src_ip.split('.')
        legal_addr = True
        # check if the src ip is an ilegal subnet ip
        if parts[0] in common_mask_starts:
            for i in range(IP_LENGTH):
                if mask[i] != '-1':
                    if parts[i] != mask[i]:
                        legal_addr = False
                        break
        if not legal_addr:
            FLAG += packet[Raw].load
print(FLAG)
