#! /usr/bin/python
from scapy.all import *
from scapy.layers.inet import ICMP

FLAG = ""


print("start")
packets = rdpcap('MuhamadAli.pcap')  # open file in pcap format

for packet in packets:
    # We're only interested packets with a ICMP layer
    if ICMP in packet:
        # the packet is part of out 1ee7 session
        if packet[ICMP].id == int("0x1ee7", 16):
            # the packet is echo request(what we sent), and not echo replay
            if packet[ICMP].type == 8:
                # take the packet's payload length and convet it to ascii
                FLAG += chr(len(packet[Raw].load))

print(FLAG)
