---
title: About us
subtitle:
comments: false
---
noxale is a local Israeli CTF team which includes only passionate teenagers. 
We create and participate in CTF events and teaching Cyber Security in our communities.

**noxale**  
*adjective*

#### definitions:
1. harm/damage/injury
2. power to harm
