---
title: Weekly Challenge
subtitle: Desmos Magic Printer
comments: false
---
Welcome to our weekly challenge!!

I found a new fantastic [tool](../../desmos/Desmos.py) who takes my formulas and makes from them a super cool graphs!

![](../../desmos/1.png)

Category: #math #scripting

To see past challenges writeups, click here!