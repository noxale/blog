---
title: "[MagTF] NothingToSee"
date: 2017-12-02T17:35:45+02:00
subtitle: "Challenge by WelloWorld"
tags: ["MagTF", "steganography"]
aliases:
  - /2017/12/magtf-nothingtosee-steg-challenge-by.html
---

NothingToSee is an easy-medium steganography task I have wrote in-order to show
a method of using visual way to see things.  
You get one PNG file, with nothing you can see.  
To solve that, you must to look on the alpha channels in the picture, you can
see that all the alphas are 0 or 1 so you save it all as a string in case it's
related to binary data.  
Then some solvers may think that it's something with the real binary data but
not, you might want to change all the alphas to 255 to see the real picture and
then you can see that what is written there is that the height is 5 and the
weight is 61.

![After](after.png)

Then, you have a rule for something but you don't know how it's connected to the
long binary raw. Like other steganographic CTFs, the alphas somewhere repeated
itself. Because it's 0's and 1's you can't just read it, so either you
understand you need (5*61) characters or make a script calculating where it
begins to repeat itself.  
After get the repeated string you may think that it means something at binary
but not, you must place them 61 weight and 5 height shape. Then you can see the
answer is STEGGY so you trying `MagTF{STEGGY}` and it works :)  

![Sol.py code](NothinToSee.png)

![Result of the script](1.png)
