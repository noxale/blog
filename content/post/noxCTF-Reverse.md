---
title: "[noxCTF] Reverse"
date: 2018-09-18T10:48:00+05:00
subtitle: "Challenge by JohnE"
tags: ["noxCTF", "forensic", "networking", "DNS"]
aliases:
  - /2018/09/noxCTF-Reverse.html
---


>Note: this is not a Reverse challenge  
Note: change the flag format from noxctf{X-X-X} to noxCTF{X_X_X}


__Writeup__  
At the beginning of the challenge we get an executable file, that looks like cmd.  
![image](/page/images/noxctf-reverse/reverse1.png)

Lets start running commands and look if we see something suspicious  
But so far it seems like normal limited cmd.  
When we call the 'netstat' command we get a constant output:  
![image](/page/images/noxctf-reverse/reverse2.png)

> Netstat- Displays protocol statistics and current TCP/IP network connections.


Lets try to connect to those ips with the currect port(maybe we will get a letter from each server), it dose not work.  
We can try some more things such as: convert the coordinates of the ips to ascii and reverse its order, and several more things that will takes us few minutes but, nothing will work. 

Ok, so we just need to understand what to do with those ips and ports.  
Lets go back to the Title of the challenge: 'Reverse'  
In which case we get an IP address as a result? -------> ```DNS```  
Great so we need to look at the `Reverse DNS`!

>A reverse DNS record (or PTR record) is simply an entry that resolves an IP address back to a host name

After short search we see that 'netstat' has a flag that can be useful:  
![image](/page/images/noxctf-reverse/reverse3.png)


It turns out that we can user 'nslookup' as well.  
According MSDN:  

> If computerTofind is an IP address and the query is for an A or PTR resource record type, the name of the computer is returned.

Now we can do 4 things:  
1. Sand manually PTR DNS requests  
2. Use socket.gethostbyaddr function(lazy🤤)  
3. Use nslookup  
4. Use netstat

So now we have domain and port:  


| domain | port |
| -------- | -------- |
|  ns355.altervista.org  |0 |
| pt7.altervista.org | 15|
| outbound-mail-211-233.reflexion.net| 27|
|mail-bl2nam020074.inbound.protection.outlook.com|31|
|sn-69-12.tll07.zoneas.eu|9|
|ispconfig.domainoftheday.com|17|
|ec2-54-225-53-118.compute-1.amazonaws.com|21|
|147.62.236.23.bc.googleusercontent.com|2|
|www.credit.com|5|
|mail-cy1nam020042.inbound.protection.outlook.com|4|
|osrs.systemdns.com|11|
|mail-qb1can010010.inbound.protection.outlook.com|10|
|ec2-52-50-45-65.eu-west-1.compute.amazonaws.com|4|
|cb-in-f26.1e100.net|2|
|mail-dm3nam030010.inbound.protection.outlook.com|20|
|mail.lasvegas.com|3|
|a23-202-89-48.deploy.static.akamaitechnologies.com|11|
|mail4.lifealert.com|16|
|edge-star-mini-shv-02-ort2.facebook.com|34|
|edge-star-shv-02-ort2.facebook.com|26|
|qv-in-f26.1e100.net|13|
|yx-in-f26.1e100.net|1|

Wait, at the first line we can see that the port is 0, strange. 

> port 0 is reserved port in TCP/IP networking, meaning that it should not be used in TCP or UDP messages.

OK, but it must be somthing...  
lets look again at what we got and think. We have names(strings) and ports(number), it must be an index.

Lets go through each domain and take the letter in the index of the port: domain[port].  
Bingo we get something like this *noxctfp7r-dn5-bl4ckb0x*.  
If we will go back to the exe output we will see:  

> 1337.1337.1337.1337:1337  
> 1.3.3.7:1337

Those two lines cannot be converted to PTR DNS.  
Maybe those lines are the symbols '{' and '}'?  
After the translation we got  *noxctf{p7r-dn5-bl4ckb0x}*  
Lets convert it to the template: `noxCTF{p7r_dn5_bl4ckb0x}`


solution:[solve_Reverse.py](/page/images/noxctf-reverse/solve_Reverse.py)