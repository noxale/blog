---
title: "[noxCTF] Sublime"
date: 2018-09-18T10:48:00+05:00
subtitle: "Challenge by G4S"
tags: ["noxCTF", "crypto"]
aliases:
  - /2018/09/noxCTF-Sublime.html
---

Sublime is a medium difficulty cryptography challenge about the substitution cipher. I made this challenge more difficult by setting the substitution range of values to the whole ASCII table.

To solve this challenge, you first need to perform a frequency analysis on the given ciphertext, omitting the variety of punctuation marks from the analysis but spaces (e.g. dots, exclamation marks, question marks etc..) and sort the frequencies from the highest to the lowest. You then need to match and swap each character, from the higher frequency to lower one, with its corresponding character from the English language frequency analysis table (can be found online), adding the character 'space' to the highest spot on that table before. The resulted text will be partially readable (all characters will be readable, though the words will be meaningless) and some guesses should be made about which letter should be which (for example, the resulting word 'tne' should be treated to as the word 'the' so the letter 'n' should be swapped with the letter 'h'). After the entire decryption of the cipher, the flag will be seen at the end of the file, where a directive will guide the solver to wrap the flag with the original wrapping (noxCTF{}). To get the final version of the flag, which is also the correct one, the solver needs to use the details from the second attached file, which guides to swap certain letters with certain numbers (1337).

The flag for this challenge is: noxCTF{r341_pr0gr4m3rs_us3_vim}