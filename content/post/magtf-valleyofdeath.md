---
title: "[MagTF] ValleyOfDeath"
date: 2017-12-06T17:52:23+02:00
subtitle: "Challenge By WelloWorld and Shahar Snitovski"
tags: ["MagTF"]
---

So this challenge is meant to be a regex challenge with some trolling and make
it more harder.
At the beginning, the user get a file named P. This file is a XZ compressed data
by tar.xz. After user decompressed the file, he will get a folder about 550
subfolders depth(e.g. subfolder 1 is inside P and subfolder 2 is inside 1 and so
on). When he gets the last file named con (zip file), he will need to open it
and see that it is another folder contains 550 subfolders, So he should make a
code that does it. In the end (after 1100 and something folders) he will see a
file named TOPSECRET.txt (TROLLING). He will be open it and see this base64: 
```
LS0gLi0gLS0uIC0gLi4tLiAjIC0gLi4uLiAuLS0tLSAtLiAtLi0gIyAuLi4gLS0gLi0gLi0uIC0gLiAuLS4gIw==
```

When he decodes it he should see that:
```
-- .- --. - ..-. # - .... .---- -. -.- # ... -- .- .-. - . .-. #
```

This is a corrupt morse code and if he will delete every # and than translate he
will see that:
```
MAGTFTH1NKSMARTER
```

And by the #'s, he should think the flag is: MagTF{TH1INK_SMARTER} BUT NOT!!
It's just a trolling. While he does all that things he should be aware that
filenames mean something, so he should gather all the filenames in a row that
looks like that:
![](1.png)

Now, he knows the flag should be `MagTF{*}`, So he should be searching for this
pattern in the whole text. First he should look for a { and }, and if he does,
he will see that the text contains one { and one }, and that each one of them is
in the middle of two '$' (e.g. '${$' , '$}$' ). Now he knows that he can find the
flag around the text of filenames. After researching he should get the following
rules of regex:

* Upper case letter - between 2 small letters from each side (e.g. 'aaPaa')
* Lower case letter - between 2 big letters from each side (e.g. 'PPaPP')
* Special characters - Dollar sign from each side (e.g. '${$', '$_$')<Paste>
* Numbers- Palindrome of !,uppercase,lowercase,letter,lowercase,uppercase,! (e.g. '!Ps5pS!')

After running all these rules with regex, he should get the flag:
```
MagTF{R1G_X_SH3N4N1G4N5}
```

Code for save all the letters and play with regex:
![sol.py](2.png)
