---
title: "[noxCTF] SUBliminal"
date: 2018-09-18T10:48:00+05:00
subtitle: "Challenge by JohnE"
tags: ["noxCTF", "pcap", "forensic", "networking"]
aliases:
  - /2018/09/noxCTF-Subliminal.html
---

**Challenge description**:  

> "SUBliminal is an Israeli rapper that eschew from Illegal things.
> (don't smoke weed everyday)".

>goo.gl/pSGubK


**special idea**:  
1.  To understand that we interested in the data of the packet that has been sent from illegal subnet ip.  
2.  To understand that illegal subnet address is an address that belong to subnet(start with 10 or 192) and don't implement the rest of the mask: 10.0.0.? 


**Writeup**:

So, at the beginning of the challenge we get a note: “SUBliminal is an Israeli rapper that eschew from Illegal things. (don't smoke weed everyday)” , and a link.
When we open the link we get to a song of subliminal at Youtube, (not important).

Ok, first lets search with wireshark filter, the packets which contains the text “subliminal” by typing in the filter box the next command- frame contains “subliminal”, we don’t see anything.
we also gonna :mag\_right:  the keywords: “sub”,”flag”,”nox”, because why not? But…….. nothing.
![image](/page/images/noxctf-subliminal/subliminal1.png)

In this position we are not sure what we are supposed to do, so its recommended to search something interesting in the files that was sent in the traffic. We can get access to these files using wireshark: ```File->Export Objects->HTTP```.
As we going through the files we see three interesting files:  

*  SUB  
*  Subliminal  
*  Subliminal.txt

When we open them we see the next message:  

> “The resource you are looking for has been removed, had its name changed, or is temporarily unavailable”:thumbsdown\_tone2:

this is an ASP.net 404 error which means that the requested file is not located in the server.
Lets check  which site has been requested for those files (maybe we need to start working with this site at this challenge). We discovered that the site is a normal high school site: alehlod.iscool.co.il.

Ok, it seems that we are in a dead end, so lets start over and try to understand better the challenge’s description.  
First the “**SUB**” in “subliminal” is 
in upper case so it probably a hint.
In addition we can also see that the author of the challenge bother to add an info about the fact that subliminal don’t use **illegal** stuff (odd).
 
Another thing that catches the eye, is that there is a DHCP traffic at the beginning of the sniffing. That means that the “sniffer” connected to a DHCP server. As we looking for something strange at the **subnet_mask** field (because after all “SUB”), we can’t find anything suspicious.   

Lets try to go to other direction. So… what we got is SUB and illegal.
Lets try to take the subnet_mask from the DHCP ACK and crosscheck it with the router IP to get the template for the legal ip addresses in the subnet. And once we have this template we can focus only in data that has been sent from packets in the subnet. 

To view the DHCP traffic in wireshark we can type the filter ```udp.port == 67``` or ```udp.port == 68``` (in DHCP handshake the client communicate over udp in port 68 and the server over port 67 :nerd: ). We see that the subnet_mask is 255.255.255.0 and the router ip is 10.0.0.138 so each host in the subnet should look like 10.0.0.X
![image](/page/images/noxctf-subliminal/subliminal2.png)

If we will write a script that takes the data(payload) that has been sent from legal subnet host we are going to get a lot of junk includes http requests. Even when we filter the http traffic we still get nothing important.

Lets try a different approach, to look at all the data that has been sent from addresses that don’t implement the template 10.0.0.X . but in a second thought those addresses are external servers so it will not give as something specific.

So what does it means illegal subnet address? Maybe we need to look at addresses that implement 10.0.0.X, but also the free coordinate(X) is above 255 or below 0. There are no such packets in the pcap file, so it must be something else.

After reading about LAN addresses we know that the most common first coordinate of LAN ip template is ***10*** or ***192***(in our case its 10). So we gonna search packets that their ip starts with 10 but the rest of the address isn’t match to the mask.

By looking in wireshark at the packets that theirs dst ip is not in our subnet we get nothing,  
but by looking at the packets with src "fake subnet" ip with the filter: ```ip.src > 10.0.0.255 && ip.src < 10.255.255.255``` we can see that the source ips are not in our subnet and the dst ips are not in our subnet as well.  
Its weird that we see packets that don't belong to our LAN, we can also see that those are **SYN** tcp packets that doesn't get any **SYN+ACK** reply, this is also strange.

Those packets are too suspicious for a pcap challenge. It **must** be direction.    

After writing a little script we see that there are packets that implement the rules we decided on , and also that they all have a payload in length of 1.
So if we take those packets payload and connect them together we are gonna get the Flag! 


> **Flag**: noxCTF{Subn37_f0r_l1f3} 

--------------------------------------------------------------------------------------------------
**NOTE:**  
This challenge was definitely harder than the rest of the forensics challenges because it required researching in a way of thinking out of the box.


```Solution```: [solve_Subliminal.py](/page/images/noxctf-subliminal/solve_Subliminal.py)