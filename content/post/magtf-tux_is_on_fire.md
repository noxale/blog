---
title: "[MagTF] Tux_is_on_fire"
date: 2017-12-06T17:58:09+02:00
subtitle: "Challenge by WelloWorld"
tags: ["MagTF", "misc"]
---

Tux_is_on_fire is an easy-medium misc task. You get a port and an ip, You can
connect with netcat and see the following:
<!--more-->

```
Dear friend, let's start with an easy task. All you need to do is to do the next steps: Atbash(Book('elixir-electrons/linux/v?.?/source/kernel', 'W1snZm9yay5jJyw3XSxbJ21vZHVsZS5jJyw1Nl0sWydleGl0LmMnLDg2XSxbJ3Jlc291cmNlLmMnLDI5XSxbJ2t0aHJlYWQuYycsNV0sWydzaWduYWwuYycsMTQxXSxbJ2dyb3Vwcy5jJywyNDAwNF0sWydjcHUuYycsMjhdLFsnZnJlZXplci5jJyw0NzJdLFsnc21wLmMnLDg2XSxbJ2NyYXNoX2R1bXAuYycsNF0sWydkbWEuYycsOF0sWydNYWtlZmlsZScsMV0sWydkbWEuYycsOV0sWydzbXAuYycsMjc2Ml0sWydtb2R1bGUuYycsM10sWydleGl0LmMnLDYzMF0sWydjcHUuYycsOV0sWydmb3JrLmMnLDI0NzY3Nl0sWydrdGhyZWFkLmMnLDJdLFsna3RocmVhZC5jJywyOF0sWydyZXNvdXJjZS5jJywzMDRdLFsnTWFrZWZpbGUnLDg2XSxbJ2V4aXQuYycsMjldLFsnZm9yay5jJyw1XSxbJ3Jlc291cmNlLmMnLDEyMTkyMV0sWydyZXNvdXJjZS5jJyw4NjBdLFsnY3Jhc2hfZHVtcC5jJywxNzY0XSxbJ3Jlc291cmNlLmMnLDUyOF0sWydtb2R1bGUuYycsMTc1OV0sWydtb2R1bGUuYycsMzE0OTQzXSxbJ2NyYXNoX2R1bXAuYycsNzU5OV1d'))
Oh shit, I forgot what kernel version I chose.. Damn. 
SUBMIT WHAT YOU GOT:
```

So we see we need to do an Atbash decryption on a book cipher on the following
base64 text: 
```
[['fork.c',7],['module.c',56],['exit.c',86],['resource.c',29],['kthread.c',5],['signal.c',141],['groups.c',24004],['cpu.c',28],['freezer.c',472],['smp.c',86],['crash_dump.c',4],['dma.c',8],['Makefile',1],['dma.c',9],['smp.c',2762],['module.c',3],['exit.c',630],['cpu.c',9],['fork.c',247676],['kthread.c',2],['kthread.c',28],['resource.c',304],['Makefile',86],['exit.c',29],['fork.c',5],['resource.c',121921],['resource.c',860],['crash_dump.c',1764],['resource.c',528],['module.c',1759],['module.c',314943],['crash_dump.c',7599]]
```

We understand that book cipher means to get the page, and get the letter in the
number place in the page. If you get all these and merge it to a string you will
get a sentence that needs to get into the Atbash cipher.
Also, to make it harder, I didn't really tell the version of the kernel on the
site I used to create that sentence, so they should run a brute force on every
version. (I used 4.3)

![](1.png)

We get the sentence:
```
printk(\"Nice! Go: %d\",int(2095))
```

printk is the printf to the kernel messages to user-space. Maybe the Challenge
connected?
We are trying to connect to 2095 port and we get a file in hex bytes, we are
saving it to a file, and we run 'file' command to see what file is it, we
discovered it is a kernel module.
Running the executable (of course on a virtual machine) and having a look in the
'dmesg' shows us the string:
```
Good job, continue: YvhgUozt3evi
```

Doing an Atbash on that, shows us the flag: `BestFlag3ver`  
Sending it to the first server, and get the real-flag: `MagTF{K3rnel_Module_1s_The_Best!<3}`
