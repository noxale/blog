---
title: "[noxCTF] Muhammad Ali"
date: 2018-09-18T10:48:00+05:00
subtitle: "Challenge by JohnE"
tags: ["noxCTF", "pcap", "forensic", "networking", "ICMPKnocking"]
aliases:
  - /2018/09/noxCTF-MuhamadAli.html
---

**Challenge description**:  

> Muhammad Ali: "I've Ceen Many Pictures of Knocking-out in my career"


**special idea**:

1)The main idea of the challenge is ICMP Knocking.  

2)This is not a Stego challenge, this is a Networking challenge!  

3)solving without understanding(frame contains "flag"/"noxctf",follow each tcp stream) will lead you to stego dead end.



__Writeup:__ 

At the beginning of the challenge we get a description:
Muhammad Ali: "I've Ceen Many Pictures of Knocking-out in my career"
ok, it seems like we need to search picture files of knockouts inside the pcap file.

Ok, but before searching things in a pcap in forensics challenges we should do several things to get a clue on what we should look closely. One of these steps is to look the "Protocol Hierarchy".
![image](/page/images/noxctf-muhamadali/muhamad1.png)  
Interesting 1.4% of ICMP traffic its suspicious considering that someone build this pcap file for us, but lets leave it.  

First, at the beginning of the traffic we can see a connection between two host
(10.0.0.3, 10.0.0.4) in LAN on port 1337(interesting).  
![image](/page/images/noxctf-muhamadali/muhamad2.png)

Lets go deeper and look at the stream of the connection by Right click->Follow-> Follow TCP Stream.  
![image](/page/images/noxctf-muhamadali/muhamad3.png)

+ There are pictures as we thought.
+ There is some Stego in those pics
+ JhonE download them.

So lets search them in the files . Wireshark allow us to download the files that was transferred in the traffic: File->Exprot Object -> http. 
After downloading them we can see some interesting pictures 

![image](/page/images/noxctf-muhamadali/muhamad4.png)

First thing first we already know that those are stegos, so lets open them with **stegsolve** and see if we can find something.

__flagc.png__:

![image](/page/images/noxctf-muhamadali/muhamad5.png)


__knockout1c.png__:

![image](/page/images/noxctf-muhamadali/muhamad6.png)


__knockout2c.png__:

![image](/page/images/noxctf-muhamadali/muhamad7.png)


__knockout3c.png__:

![image](/page/images/noxctf-muhamadali/muhamad8.png)


__knockout4c.png__:

![image](/page/images/noxctf-muhamadali/muhamad9.png)

We have been trolled and it was a waste of time. So..... whats now?

Lets try to go back to the challenge's description and try to see if we missed something there.

"__I__'ve **C**een **M**any **P**ictures of __Knocking__-out in my career". 
Its much more clear now ICMP Knocking!

> ICMP Knocking: A method that constitute authentication part before connection to host in specific destination port. The implementation of ICMP knocking includes sending ICMP echo request(ping) with payload in different length and in different order. If the sequence  is correct, the destination host will open the requested port and then we can communicate over this port!

After filtering the traffic to get only the ICMP packets we got this:  

![image](/page/images/noxctf-muhamadali/muhamad10.png)

Ok, lets go deeper and look at the packets payload. We see that several packet have randomize payload (it saids us nothing), and the other packets have a constant payload "JhonE", "yonatanerez".

There are packets that being sent to same IP address with different ID, so that means that those packets are not belongs to each other------> Not ICMP knocking.

If we look deeper we will see that all the packets with 216.58.206.110 as destation ip have the same ID- ```1ee7```.  
More over, we can see that all the packets with 8.8.8.8(google dns server) or 105.99.109.112(ascii representation of "icmp") as a dst ip have a constant length of payload, so its probably not an ICMP Knocking.

So, lets write a script that pass through all the icmp echo packets with the 1ee7 ID, and prints the payloads lengths so we will get the knocking sequence that will enable us to connect to the server.
After that we are gonna look at the connection between 10.0.0.3 and 216.58.206.110, and we see that the is a knocking but no connection was created.

We will keep looking at the payloads length and discover that all the lengths are in a range of 0-255(ascii).
We will convert those length to ascii and get the flag!!!

> **Flag**: noxCTF{kn0ck1n6_my_1cmp}

```Solve script```: [solve_MuhamadAli.py](/page/images/noxctf-muhamadali/solve_MuhamadAli.py)