---
title: "[MagTF] rEVEenge!"
date: 2017-12-06T17:46:53+02:00
subtitle: "Challenge by WelloWorld"
tags: ["MagTF"]
aliases:
  - /2017/12/magtf-reveenge-challenge-by-yanporan.html
---

Hey there. The solvers start the challenge as they open the folder contains pcap
file, and an ELF file. IMPORTANT NOTE: This ELF is not to be reversed although I
made some encryptions to the strings and even then it would be hard to deal with.

As the run the ELF they see the next lines written by Eve, Bob first love
(before Alice): 

>Hey Bob, Its EVE. Do you remember me? your first love, your first girlfriend
>'til Alice came..  
>If you ever want to see her again you'll need to get my special eve-serie
>inorder to free the flag.  
>By the way, if you don't believe - I'm sending you the pcap of me malformed her.  
>Have fun!  
>PLEASE ENTER TO CONTINUE:  

They understand that Eve has kidnapped Alice and Bob need to put some serie to
get the flag. Then comes the first clue says that 'malformed her' (maybe will
help). Then when they press the Enter button the fun begins.. their screen
started to get filled with more characters you can imagine (They can wait it to
be stopped but it wont happen). After maybe sometime they play in the random
strings the ELF prints to the screen they would decide to get out, so they will
press ctrl-z or ctrl-d but wait, it's not work! In the code, I tried to catch
every signal (there are about 31-32) and save it to some data variable that saves
all the signals. To make the challenge more hard and tricky, The code does an
exit() when he receives 20 signals. So now the user understand there something
with the signals, and should get the exit from before(example: run every signal
to see what happens). He will be stuck here unless he will reverse the code (BUT
WE DON'T WANT HIM TO DO THAT! ITS NOT THE CHALLENGE). So I'm guessing he would
try the pcap file, In the pcap file you can see some weird packets like that
(second,third and forth packets):

![Screenshot of WireShark](1.png)


You can see that there is a malformed packet! like EVE told us. You enter to it
and see that the packet ends with "*K", maybe it's important. You probably try
to see the next packet and HEY, you can see it end with "*n", and you continue
and continue and then you will see that even the IP's are different and the
packet itself is not the same packet there is some pattern with the letters and
*, so you maybe want to enter some filter to wireshark (or BPF) to show only
packets contains "" and then letters. After you find all the packets you can see
when you concatenate all the letters, we get the string: "KnockingMaster". What
does it mean? maybe you would try to put that in MagTF form but not it wont work.
You either know the answer or search in the internet action called "port
knocking". That means as it sounds, just knock on the port for see if they will
answer back. So you would think about ports and you will discover that maybe the
packets with * are sent to some important ports? and you gather all the ports
and you get a SERIE of : [6,23,13,4,11,8,20,7,3,2,26,5,12,18]. You may think it
contains some data in encoding or some easy encryption but you can see all the
number are below 26... and some how, it becomes relative to the ELF. You should
be trying to enter signals as I will show in the solution and HOP, here you go,
we get the flag says `MagTF{You_wish_so}`. So Alice wont be free soon, but hey,
at least they succeed the challenge!

Solution.sh - a bash script to kill as the sequence we got(sleep is for let the signal to rest a little bit,it works both ways):

![Killer](killer.png)
