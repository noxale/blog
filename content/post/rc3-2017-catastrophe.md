---
title: "[RC3-2017 CTF] Catastrophe"
date: 2017-12-01T17:20:28+02:00
subtitle: "Writeup by WelloWorld"
tags: ["rc3 2017", "forensic"]
aliases:
  - /2017/11/rc4-2017-paranoid-forensic-challenge.html
---

In the beginning, you get a Pcap file that contains 3200 packets of data. The
description told us that while one friend went to somewhere and forgot his
computer open, his friend took his computer and entered to sites of cat pics.
He want to know more about what his friend did and luckily for him, he forgot
his Wireshark on. So we know we’re searching for cat pics right now. The first
thing I did was to search if some packets contain cat.png or cat.jpg: 

![Wireshark Screenshot](1.png)

Oh, look at these two packets, Now lets look on their ‘follow tcp-stream’:

![HTTP Request Screenshot](2.png)
![HTTP Request Screenshot](3.png)

Now, which one is more relevant to us? Of course the second one, Lets have a
look on this after converting it to ‘Raw’ bytes.

![Raw Packet Screenshot](4.png)

You can see there are a start of jpg file (‘ffd8’) right in the start of the
second blue packet. Now let’s find where it ends('ffd9'):

![Raw Packet Screenshot](5.png)

Right in the end of the conversation.  
OK, lets filter it only for what the server sent to me, stay in ‘Raw’ mode and
copy all the data from the screen and paste it in a local file on my computer.
Now, we can see that the headers of the packet are still there and we must
delete them to make that picture again:

![Raw Packet in Sublime Text](6.png)

Then, we need to two things, first- delete every new line, because of the
copy-paste from the Wireshark raw, and second- run on the hexadecimal we found,
make every two characters to a real hex, and write them in a binary file. I made
a python script to do it:

![Python Code](7.png)

And an image file named flag created in the same directory:

![A Picture of a Cat With The Flag](8.jpg)
