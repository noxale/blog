---
title: "[MagTF] Crypto1sFun"
date: 2017-12-02T12:48:00+02:00
subtitle: "Challenge by WelloWorld"
tags: ["MagTF", "crypto", "forensic"]
aliases:
  - /2017/12/magtf-crypto1sfun-by-welloworld.html
---

So, this is a medium-hard crypto challenge with a little bit of forensic. The
solver gets a server and port and a description says: 

>"Today you will learn to work with two important mechanisms: The first one is very known today as a good mechanism, and the second one is old and not used today as normal mechanism."

The mechanisms are: Deffie-Hellman key exchange and the WEP for wifi
authentication (wifi is not really here). If they try to connect through sockets
to the server, they will find out it sends to them the string:

>"Wello Bob!\ng=<num>\np=<num>\n" 

With replacing with real random numbers so now they have g and p values. They
will try some things but at the end when Alice sends g and p to Bob she expects
to get her b(actually B but it gets it too) value. So when the user sends b= the
server will send him the A of him and each one of them need to calculate the key
by the values they got.

After that the server will send to the socket a little clue about WEP (and
ciphertext and IV to be decipher) that says: 

>"HAHAHA IEEE WOH"

(The clue is IEEE by the way), So they get from the packet two things: the
ciphertext and the IV for the decryption. With the previous key (DH) they
calculated as string of key , they need calculate the plaintext by WEP mechanism
(KSA,PRGA, XORing) and they get the string "MagTF{Deffie_Helman_+_WEP=FUN}" and
a little bit of crc32 because of the WEP mechanism. 

**Example:**  
Connect to the host:port then:

Server:
```
g=101
p=181
```

Client:
```
b= 736 #private number
B=39 #(101**736)%181=39
```

Server:
```
a= 465 #private number
A=133 #(B**a)%181
```

The key is 48 (either  (133**736)%181 or (39**465)%181)  
Then the client recieves the message:  
\> Hehehe ieee woh, decrypt it:  
\> {Encrypted message}  
\> {Init vector}  

end of connection

The solution after getting the key of DH is(after the server sent the data):
![Solution](CryptoIsFun.png)
